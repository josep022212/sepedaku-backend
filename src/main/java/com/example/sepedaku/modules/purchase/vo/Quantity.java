package com.example.sepedaku.modules.purchase.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Quantity {
    @Column(
            name = "quantity"
    )
    private int quantity;

    public Quantity(int quantity) throws ValidationException {
        this.quantity = quantity;
    }
}