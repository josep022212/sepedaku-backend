package com.example.sepedaku.modules.purchase.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
@Getter
@NoArgsConstructor
public class PurchaseDate {
    @Column(
            name = "purchase_date"
    )
    private Date purchaseDate;

    public PurchaseDate(Date purchaseDate) throws ValidationException {
        this.purchaseDate = purchaseDate;
    }
}