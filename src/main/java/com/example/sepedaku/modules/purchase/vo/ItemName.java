package com.example.sepedaku.modules.purchase.vo;
import com.example.sepedaku.exception.ValidationException;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class ItemName {
    @Column(
            name = "item_name"
    )
    private String itemName;

    public ItemName(String itemName) throws ValidationException {
        this.itemName = itemName;
    }
}