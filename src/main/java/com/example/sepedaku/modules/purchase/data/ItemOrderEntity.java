package com.example.sepedaku.modules.purchase.data;

import com.example.sepedaku.modules.purchase.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "item_orders")
@SequenceGenerator(name = "item_order_gen", sequenceName = "item_order_id_gen", allocationSize = 1)
public class ItemOrderEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "item_order_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private ItemName itemName;

    @Embedded
    private Price price;

    @Embedded
    private Quantity quantity;

    @ManyToOne
    @JoinColumn(name = "purchase_order_id")
    private PurchaseOrderEntity purchaseOrder;
}
