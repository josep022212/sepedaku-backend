package com.example.sepedaku.modules.purchase.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Price {
    @Column(
            name = "price"
    )
    private float price;

    public Price(float price) throws ValidationException {
        this.price = price;
    }
}