package com.example.sepedaku.modules.purchase.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class VendorName {
    @Column(
            name = "vendor_name"
    )
    private String vendorName;

    public VendorName(String vendorName) throws ValidationException {
        this.vendorName = vendorName;
    }
}