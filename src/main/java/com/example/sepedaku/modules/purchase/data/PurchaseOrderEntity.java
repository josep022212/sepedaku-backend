package com.example.sepedaku.modules.purchase.data;

import com.example.sepedaku.modules.purchase.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "purchase_orders")
@SequenceGenerator(name = "purchase_order_gen", sequenceName = "purchase_order_id_gen", allocationSize = 1)
public class PurchaseOrderEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "purchase_order_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private PurchaseDate purchaseDate;

    @Embedded
    private Status status;

    @Embedded
    private VendorName vendorName;

    @OneToMany(mappedBy = "purchaseOrder")
    private List<ItemOrderEntity> itemOrder;
}
