package com.example.sepedaku.modules.admin.controller;

import com.example.sepedaku.modules.admin.data.AdminEntity;
import com.example.sepedaku.modules.admin.dto.AdminDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admins")
@AllArgsConstructor
public class AdminController {

    @GetMapping("{id}")
    public AdminDTO get(
            @PathVariable("id") AdminEntity admin
    ) throws Exception{
        return new AdminDTO(admin);
    }
}
