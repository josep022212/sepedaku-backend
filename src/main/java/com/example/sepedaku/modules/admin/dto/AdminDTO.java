package com.example.sepedaku.modules.admin.dto;

import com.example.sepedaku.base.enumerator.UserTypeEnum;
import com.example.sepedaku.modules.admin.data.AdminEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminDTO {
    private Integer id;
    private String email;
    private String gender;
    private String name;
    private String phoneNumber;
    private String username;
    private String position;
    private String type;

    public AdminDTO(AdminEntity entity){
        if(entity == null) return;

        id = entity.getId();
        email = entity.getEmail().getEmail();
        gender = entity.getGender().getGender();
        name = entity.getName().getName();
        phoneNumber = entity.getPhoneNumber().getPhoneNumber();
        username = entity.getUsername().getUsername();
        position = entity.getPosition().getPosition();
        type = UserTypeEnum.ADMIN.getUserType();
    }
}
