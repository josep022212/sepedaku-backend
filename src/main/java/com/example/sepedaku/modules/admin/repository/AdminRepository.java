package com.example.sepedaku.modules.admin.repository;

import com.example.sepedaku.modules.admin.data.AdminEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AdminRepository extends JpaRepository<AdminEntity, Integer>, JpaSpecificationExecutor<AdminEntity> {
    boolean existsByUsername_Username(String username);

    boolean existsByUsername_UsernameAndPassword_Password(String username, String password);

    AdminEntity findFirstByUsername_UsernameAndPassword_Password(String username, String password);
}
