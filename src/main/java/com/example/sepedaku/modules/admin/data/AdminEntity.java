package com.example.sepedaku.modules.admin.data;

import com.example.sepedaku.base.entities.UserEntity;
import com.example.sepedaku.modules.admin.vo.Position;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "admins")
@SequenceGenerator(name = "admin_gen", sequenceName = "admin_id_gen", allocationSize = 1)

public class AdminEntity extends UserEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "admin_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private Position position;

//    @Embedded
//    private PurchaseOrder purchaseorder;
}
