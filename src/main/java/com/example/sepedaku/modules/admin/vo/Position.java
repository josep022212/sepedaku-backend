package com.example.sepedaku.modules.admin.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor

public class Position {
    @Column(
            name = "position"
    )
    private String position;

    public Position(String position) throws ValidationException {
        this.position = position;
    }
}
