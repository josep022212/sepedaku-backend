package com.example.sepedaku.modules.sales.repository;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.sales.data.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer>, JpaSpecificationExecutor<PaymentEntity> {
    List<PaymentEntity> findByCreditCardInfo(CreditCardInfoEntity creditCardInfo);

}
