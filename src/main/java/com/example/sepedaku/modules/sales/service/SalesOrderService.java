package com.example.sepedaku.modules.sales.service;

import com.example.sepedaku.modules.cart.data.CartEntity;
import com.example.sepedaku.modules.cart.dto.CartDTO;
import com.example.sepedaku.modules.cart.service.CartService;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.sales.data.PaymentEntity;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import com.example.sepedaku.modules.sales.dto.CreateSalesOrderDTO;
import com.example.sepedaku.modules.sales.dto.SalesOrderDTO;
import com.example.sepedaku.modules.sales.dto.UpdateStatusSalesOrderDTO;
import com.example.sepedaku.modules.sales.repository.PaymentRepository;
import com.example.sepedaku.modules.sales.repository.SalesOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class SalesOrderService {
    private final SalesOrderRepository salesOrderRepository;
    private final PaymentRepository paymentRepository;
    private final CartService cartService;

    public SalesOrderDTO get(SalesOrderEntity salesOrder) throws Exception{
        CartDTO cartDTO = cartService.getCartItem(salesOrder.getCustomer());

        return new SalesOrderDTO(salesOrder, cartDTO);
    }

    public SalesOrderDTO save(CreateSalesOrderDTO dto, CustomerEntity customer) throws Exception{
        PaymentEntity payment = paymentRepository.getById(dto.getPaymentId());
        CartEntity cart = cartService.getCart(customer);

        SalesOrderEntity salesOrder = new SalesOrderEntity(dto, cart, payment, customer);
        return get(salesOrderRepository.saveAndFlush(salesOrder));
    }

    public SalesOrderDTO update(UpdateStatusSalesOrderDTO dto, SalesOrderEntity salesOrder) throws Exception{
        salesOrder.mapUpdate(dto);

        return get(salesOrderRepository.saveAndFlush(salesOrder));
    }

    public List<SalesOrderDTO> find(Specification<SalesOrderEntity> specs) throws Exception{
        List<SalesOrderEntity> salesOrders = salesOrderRepository.findAll(specs, Sort.by("id"));
        List<SalesOrderDTO> dtos = new ArrayList<>();

        for (SalesOrderEntity salesOrder: salesOrders) {
            SalesOrderDTO dto = get(salesOrder);
            dtos.add(dto);
        }

        return dtos;
    }
}
