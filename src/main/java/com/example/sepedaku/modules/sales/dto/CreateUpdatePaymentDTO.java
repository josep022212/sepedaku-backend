package com.example.sepedaku.modules.sales.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdatePaymentDTO {
    private Float totalPrice;
    private Integer creditCardInfoId;
}
