package com.example.sepedaku.modules.sales.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateSalesOrderDTO {
    private Integer paymentId;
    private String date;
    private String paymentMethod;
    private String receiverName;
    private String receiverPhoneNumber;
    private String shippingAddress;
    private String shippingMethod;
    private Float totalPrice;
}
