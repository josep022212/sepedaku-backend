package com.example.sepedaku.modules.sales.data;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.sales.dto.CreateUpdatePaymentDTO;
import com.example.sepedaku.modules.sales.vo.TotalPrice;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "payments")
@SequenceGenerator(name = "payment_gen", sequenceName = "payment_id_gen", allocationSize = 1)
public class PaymentEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "payment_gen"
    )
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(
            name = "credit_cart_info_id",
            referencedColumnName = "id"
    )
    private CreditCardInfoEntity creditCardInfo;

    @Embedded
    private TotalPrice totalPrice;

    public PaymentEntity(
            CreateUpdatePaymentDTO dto,
            CreditCardInfoEntity creditCardInfo
    ) throws Exception{
        totalPrice = new TotalPrice(dto.getTotalPrice());
        this.creditCardInfo = creditCardInfo;
    }

    public void mapUpdate(
            CreateUpdatePaymentDTO dto
    ) throws Exception{
        totalPrice = dto.getTotalPrice() == null ? totalPrice : new TotalPrice(dto.getTotalPrice());
    }
}
