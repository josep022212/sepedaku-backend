package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class PaymentMethod {
    @Column(
            name = "payment_method"
    )
    private String paymentMethod;

    public PaymentMethod(String paymentMethod) throws ValidationException {
        this.paymentMethod = paymentMethod;
    }
}
