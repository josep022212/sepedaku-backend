package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Status {
    @Column(
            name = "status"
    )
    private String status;

    public Status(String status) throws ValidationException {
        this.status = status;
    }
}
