package com.example.sepedaku.modules.sales.data;

import com.example.sepedaku.modules.cart.data.CartEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.sales.dto.CreateSalesOrderDTO;
import com.example.sepedaku.modules.sales.dto.UpdateStatusSalesOrderDTO;
import com.example.sepedaku.modules.sales.enumerator.StatusEnum;
import com.example.sepedaku.modules.sales.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "sales_orders")
@SequenceGenerator(name = "sales_order_gen", sequenceName = "sales_order_id_gen", allocationSize = 1)
public class SalesOrderEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sales_order_gen"
    )
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(
            name = "category_id",
            referencedColumnName = "id"
    )
    private CartEntity cart;

    @OneToOne
    @JoinColumn(
            name = "payment_id",
            referencedColumnName = "id"
    )
    private PaymentEntity payment;

    @Embedded
    private Date date;

    @Embedded
    private PaymentMethod paymentMethod;

    @Embedded
    private ReceiverName receiverName;

    @Embedded
    private ReceiverPhoneNumber receiverPhoneNumber;

    @Embedded
    private ShippingAddress shippingAddress;

    @Embedded
    private ShippingMethod shippingMethod;

    @Embedded
    private Status status;

    @Embedded
    private TotalPrice totalPrice;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public SalesOrderEntity(
        CreateSalesOrderDTO dto,
        CartEntity cart,
        PaymentEntity payment,
        CustomerEntity customer
    ) throws Exception{
        this.cart = cart;
        this.payment = payment;
        date = new Date(dto.getDate());
        paymentMethod = new PaymentMethod(dto.getPaymentMethod());
        receiverName = new ReceiverName(dto.getReceiverName());
        receiverPhoneNumber = new ReceiverPhoneNumber(dto.getReceiverPhoneNumber());
        shippingAddress = new ShippingAddress(dto.getShippingAddress());
        shippingMethod = new ShippingMethod(dto.getShippingMethod());
        status = new Status(StatusEnum.ON_PROCESS.getStatusEnum());
        totalPrice = new TotalPrice(dto.getTotalPrice());
        this.customer = customer;
    }

    public void mapUpdate(
            UpdateStatusSalesOrderDTO dto
    ) throws Exception{
        status = new Status(StatusEnum.validateStatus(dto.getStatus()).getStatusEnum());
    }
}
