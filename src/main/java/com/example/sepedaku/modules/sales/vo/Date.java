package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import com.example.sepedaku.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Date {
    @Column(
            name = "date"
    )
    private java.util.Date date;

    public Date(String date) throws ValidationException {
        if(date == null || date.trim().isEmpty()) {
            this.date = null;
        }else{
            try{
                this.date = java.sql.Date.valueOf(date);
            }catch (Exception e){
                throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_VALUE_INVALID.getMessage(),"date"));
            }
        }
    }
}
