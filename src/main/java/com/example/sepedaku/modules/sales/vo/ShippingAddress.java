package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class ShippingAddress {
    @Column(
            name = "shipping_address"
    )
    private String shippingAddress;

    public ShippingAddress(String shippingAddress) throws ValidationException {
        this.shippingAddress = shippingAddress;
    }
}
