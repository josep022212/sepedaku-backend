package com.example.sepedaku.modules.sales.service;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.customer.repository.CreditCardInfoRepository;
import com.example.sepedaku.modules.sales.data.PaymentEntity;
import com.example.sepedaku.modules.sales.dto.CreateUpdatePaymentDTO;
import com.example.sepedaku.modules.sales.dto.PaymentDTO;
import com.example.sepedaku.modules.sales.repository.PaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final CreditCardInfoRepository creditCardInfoRepository;

    public PaymentDTO save(CreateUpdatePaymentDTO dto) throws Exception{
        CreditCardInfoEntity creditCardInfo = creditCardInfoRepository.findById(dto.getCreditCardInfoId()).orElse(null);
        PaymentEntity payment = new PaymentEntity(dto, creditCardInfo);
        return new PaymentDTO(paymentRepository.saveAndFlush(payment));
    }

    public List<PaymentDTO> findAll() throws Exception{
        List<PaymentEntity> payments = paymentRepository.findAll();
        return payments
                .stream()
                .map(entity -> {
            return new PaymentDTO(entity);
        }).collect(Collectors.toList());
    }

    public List<PaymentDTO> findByCreditCard(CreditCardInfoEntity creditCardInfo) throws Exception{
        List<PaymentEntity> payments = paymentRepository.findByCreditCardInfo(creditCardInfo);
        return payments
                .stream()
                .map(entity -> {
                    return new PaymentDTO(entity);
                }).collect(Collectors.toList());
    }

    public PaymentDTO update(CreateUpdatePaymentDTO dto, PaymentEntity payment) throws Exception{
        payment.mapUpdate(dto);
        return new PaymentDTO(paymentRepository.saveAndFlush(payment));
    }

    public PaymentDTO delete(PaymentEntity payment) throws Exception{
        PaymentDTO dto = new PaymentDTO(payment);
        paymentRepository.delete(payment);
        return dto;
    }
}
