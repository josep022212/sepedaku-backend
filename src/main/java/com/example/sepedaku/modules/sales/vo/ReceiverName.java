package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class ReceiverName {
    @Column(
            name = "receiver_name"
    )
    private String receiverName;

    public ReceiverName(String receiverName) throws ValidationException {
        this.receiverName = receiverName;
    }
}
