package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class ShippingMethod {
    @Column(
            name = "shipping_method"
    )
    private String shippingMethod;

    public ShippingMethod(String shippingMethod) throws ValidationException {
        this.shippingMethod = shippingMethod;
    }
}
