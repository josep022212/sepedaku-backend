package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class TotalPrice {
    @Column(
            name = "total_price"
    )
    private Float totalPrice;

    public TotalPrice(Float totalPrice) throws ValidationException {
        this.totalPrice = totalPrice;
    }
}
