package com.example.sepedaku.modules.sales.dto;

import com.example.sepedaku.modules.customer.dto.CreditCardInfoDTO;
import com.example.sepedaku.modules.sales.data.PaymentEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDTO {
    private Integer id;
    private Float totalPrice;
    private CreditCardInfoDTO creditCardInfo;

    public PaymentDTO(PaymentEntity entity){
        if (entity == null) return;

        id = entity.getId();
        totalPrice = entity.getTotalPrice().getTotalPrice();
        creditCardInfo = new CreditCardInfoDTO(entity.getCreditCardInfo());
    }
}
