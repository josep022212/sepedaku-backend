package com.example.sepedaku.modules.sales.controller;

import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.item.data.ItemEntity;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import com.example.sepedaku.modules.sales.dto.CreateSalesOrderDTO;
import com.example.sepedaku.modules.sales.dto.SalesOrderDTO;
import com.example.sepedaku.modules.sales.dto.UpdateStatusSalesOrderDTO;
import com.example.sepedaku.modules.sales.service.SalesOrderService;
import com.sipios.springsearch.anotation.SearchSpec;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales-order")
@AllArgsConstructor
public class SalesOrderController {
    private final SalesOrderService salesOrderService;

    @PostMapping("/customers/{userId}")
    public SalesOrderDTO save(
            @PathVariable("userId")CustomerEntity customer,
            @RequestBody CreateSalesOrderDTO dto
    ) throws Exception{
        return salesOrderService.save(dto, customer);
    }

    @GetMapping("/{id}")
    public SalesOrderDTO get(
            @PathVariable("id")SalesOrderEntity salesOrder
    ) throws Exception{
        return salesOrderService.get(salesOrder);
    }

    @PutMapping("/{id}")
    public SalesOrderDTO update(
            @PathVariable("id")SalesOrderEntity salesOrder,
            @RequestBody UpdateStatusSalesOrderDTO dto
    ) throws Exception{
        return salesOrderService.update(dto, salesOrder);
    }

    @GetMapping()
    public List<SalesOrderDTO> find(
            @SearchSpec Specification<SalesOrderEntity> specs
    ) throws Exception{
        return salesOrderService.find(specs);
    }
}
