package com.example.sepedaku.modules.sales.enumerator;

import com.example.sepedaku.base.enumerator.GenderEnum;
import lombok.Getter;

@Getter
public enum StatusEnum {
    ON_PROCESS("ON_PROCESS"), PACKING("PACKING"), SHIPPING("SHIPPING"), DONE("DONE");
    private String statusEnum;

    StatusEnum(String statusEnum){
        this.statusEnum = statusEnum;
    }

    public static StatusEnum validateStatus(String statusEnum) {
        switch (statusEnum) {
            case "ON_PROCESS":
                return StatusEnum.ON_PROCESS;

            case "PACKING":
                return StatusEnum.PACKING;

            case "SHIPPING":
                return StatusEnum.SHIPPING;

            case "DONE":
                return StatusEnum.DONE;

            default:
                throw new IllegalArgumentException("status [" + statusEnum  + "] not supported.");
        }
    }
}
