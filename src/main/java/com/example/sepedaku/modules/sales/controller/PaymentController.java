package com.example.sepedaku.modules.sales.controller;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.sales.data.PaymentEntity;
import com.example.sepedaku.modules.sales.dto.CreateUpdatePaymentDTO;
import com.example.sepedaku.modules.sales.dto.PaymentDTO;
import com.example.sepedaku.modules.sales.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payments")
@AllArgsConstructor
public class PaymentController {
    private final PaymentService paymentService;

    @PostMapping()
    public PaymentDTO save(
            @RequestBody CreateUpdatePaymentDTO dto
    ) throws Exception{
        return paymentService.save(dto);
    }

    @GetMapping()
    public List<PaymentDTO> findAll() throws Exception{
        return paymentService.findAll();
    }

    @GetMapping("/ccinfos/{ccinfoId}")
    public List<PaymentDTO> findByCCInfo(
            @PathVariable("ccinfoId")CreditCardInfoEntity creditCardInfo
    ) throws Exception{
        return paymentService.findByCreditCard(creditCardInfo);
    }

    @PutMapping("/{id}")
    public PaymentDTO update(
            @PathVariable("id")PaymentEntity payment,
            @RequestBody CreateUpdatePaymentDTO dto
    ) throws Exception{
        return paymentService.update(dto, payment);
    }

    @DeleteMapping("/{id}")
    public PaymentDTO delete(
            @PathVariable("id")PaymentEntity payment
    ) throws Exception{
        return paymentService.delete(payment);
    }
}
