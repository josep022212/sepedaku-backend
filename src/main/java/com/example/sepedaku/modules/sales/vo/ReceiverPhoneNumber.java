package com.example.sepedaku.modules.sales.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class ReceiverPhoneNumber {
    @Column(
            name = "receiver_phone_number"
    )
    private String receiverPhoneNumber;

    public ReceiverPhoneNumber(String receiverPhoneNumber) throws ValidationException {
        this.receiverPhoneNumber = receiverPhoneNumber;
    }
}
