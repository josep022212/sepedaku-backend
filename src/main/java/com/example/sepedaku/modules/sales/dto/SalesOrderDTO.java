package com.example.sepedaku.modules.sales.dto;

import com.example.sepedaku.modules.cart.dto.CartDTO;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalesOrderDTO {
    private Integer id;
    private CartDTO cart;
    private PaymentDTO payment;
    private String date;
    private String paymentMethod;
    private String receiverName;
    private String receiverPhoneNumber;
    private String shippingAddress;
    private String shippingMethod;
    private String status;
    private Float totalPrice;
    private CustomerDTO customer;

    public SalesOrderDTO(SalesOrderEntity entity, CartDTO cart){
        if (entity == null) return;

        id = entity.getId();
        this.cart = cart;
        payment = new PaymentDTO(entity.getPayment());
        date = entity.getDate().getDate().toString();
        paymentMethod = entity.getPaymentMethod().getPaymentMethod();
        receiverName = entity.getReceiverName().getReceiverName();
        receiverPhoneNumber = entity.getReceiverPhoneNumber().getReceiverPhoneNumber();
        shippingAddress = entity.getShippingAddress().getShippingAddress();
        shippingMethod = entity.getShippingMethod().getShippingMethod();
        status = entity.getStatus().getStatus();
        totalPrice = entity.getTotalPrice().getTotalPrice();
        customer = new CustomerDTO(entity.getCustomer());
    }
}
