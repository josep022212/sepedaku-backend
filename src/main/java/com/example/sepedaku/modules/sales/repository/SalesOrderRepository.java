package com.example.sepedaku.modules.sales.repository;

import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SalesOrderRepository extends JpaRepository<SalesOrderEntity, Integer>, JpaSpecificationExecutor<SalesOrderEntity> {

}
