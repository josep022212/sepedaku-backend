package com.example.sepedaku.modules.item.controller;

import com.example.sepedaku.modules.item.data.ItemEntity;
import com.example.sepedaku.modules.item.dto.CreateUpdateItemDTO;
import com.example.sepedaku.modules.item.dto.ItemDTO;
import com.example.sepedaku.modules.item.service.ItemService;
import com.sipios.springsearch.anotation.SearchSpec;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/items")
@AllArgsConstructor
public class ItemController {
    private final ItemService itemService;

    @PostMapping()
    public ItemDTO save(
            @RequestBody CreateUpdateItemDTO dto
    ) throws Exception{
        return itemService.save(dto);
    }

    @GetMapping("/{id}")
    public ItemDTO get(
            @PathVariable("id") ItemEntity item
    ) throws Exception{
        return itemService.get(item);
    }

    @GetMapping()
    public List<ItemDTO> find(
            @SearchSpec Specification<ItemEntity> specs
    ) throws Exception{
        return itemService.find(specs);
    }

    @PutMapping("/{id}")
    public ItemDTO update(
            @PathVariable("id") ItemEntity item,
            @RequestBody CreateUpdateItemDTO dto
    ) throws Exception{
        item.mapUpdate(dto);
        return itemService.update(item);
    }

    @DeleteMapping("/{id}")
    public ItemDTO delete(
            @PathVariable("id") ItemEntity item
    ) throws Exception {
        return itemService.delete(item);
    }
}
