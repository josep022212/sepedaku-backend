package com.example.sepedaku.modules.item.vo;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Category {
    @Column(
            name = "category"
    )
    private String category;

    public Category(String category) throws ValidationException {
        this.category = category;
    }
}
