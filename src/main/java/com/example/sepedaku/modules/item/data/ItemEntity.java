package com.example.sepedaku.modules.item.data;
import com.example.sepedaku.modules.cart.data.CartItemEntity;
import com.example.sepedaku.modules.item.dto.CreateUpdateItemDTO;
import com.example.sepedaku.modules.item.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "items")
@SequenceGenerator(name = "item_gen", sequenceName = "item_id_gen", allocationSize = 1)
public class ItemEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "item_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private Category category;

    @Embedded
    private Description description;

    @Embedded
    private Name name;

    @Embedded
    private Quantity quantity;

    @Embedded
    private Image image;

    @Embedded
    private SellPrice sellPrice;

    @OneToMany(mappedBy = "item")
    private List<CartItemEntity> carts;

    public ItemEntity(
            CreateUpdateItemDTO dto
    ) throws Exception {
        category = new Category(dto.getCategory());
        image = new Image(dto.getImage());
        description = new Description(dto.getDescription());
        name = new Name(dto.getName());
        quantity = new Quantity(dto.getQuantity());
        sellPrice = new SellPrice(dto.getSellPrice());
    }

    public void mapUpdate(
            CreateUpdateItemDTO dto
    ) throws Exception{
        category = dto.getCategory() == null ? category : new Category(dto.getCategory());
        description = dto.getDescription() == null ? description : new Description(dto.getDescription());
        image = dto.getImage() == null ? image : new Image(dto.getImage());
        name = dto.getName() == null ? name : new Name(dto.getName());
        quantity = dto.getQuantity() == null ? quantity : new Quantity(dto.getQuantity());
        sellPrice = dto.getSellPrice() == null ? sellPrice : new SellPrice(dto.getSellPrice());
    }
}
