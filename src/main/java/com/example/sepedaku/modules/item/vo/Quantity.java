package com.example.sepedaku.modules.item.vo;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Quantity {
    @Column(
            name = "quantity"
    )
    private Integer quantity;

    public Quantity(Integer quantity) throws ValidationException {
        this.quantity = quantity;
    }
}

