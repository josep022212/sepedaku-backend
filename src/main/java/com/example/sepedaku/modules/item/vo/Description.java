package com.example.sepedaku.modules.item.vo;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Description {
    @Column(
            name = "description"
    )
    private String description;

    public Description(String description) throws ValidationException {
        this.description = description;
    }
}
