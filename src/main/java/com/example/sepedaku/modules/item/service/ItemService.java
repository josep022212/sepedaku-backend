package com.example.sepedaku.modules.item.service;

import com.example.sepedaku.modules.item.data.ItemEntity;
import com.example.sepedaku.modules.item.dto.CreateUpdateItemDTO;
import com.example.sepedaku.modules.item.dto.ItemDTO;
import com.example.sepedaku.modules.item.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ItemService {
    private final ItemRepository itemRepository;

    public ItemDTO save(CreateUpdateItemDTO dto) throws Exception{
        ItemEntity newItem = new ItemEntity(dto);

        return new ItemDTO(itemRepository.saveAndFlush(newItem));
    }

    public ItemDTO get(ItemEntity item) throws Exception{
        return new ItemDTO(item);
    }

    public List<ItemDTO> find(Specification<ItemEntity> specs) throws Exception{
        List<ItemEntity> items = itemRepository.findAll(specs);
        return items
                .stream()
                .map(entity -> {
                    ItemDTO dto = new ItemDTO(entity);
                    return dto;
                }).collect(Collectors.toList());
    }

    public ItemDTO update(ItemEntity updatedItem) throws Exception{
        return new ItemDTO(itemRepository.saveAndFlush(updatedItem));
    }

    public ItemDTO delete(ItemEntity item) throws Exception{
        itemRepository.delete(item);
        return new ItemDTO(item);
    }
}
