package com.example.sepedaku.modules.item.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Image {
    @Column(
            name = "image",
            columnDefinition = "text"
    )
    private String image;

    public Image(String image) throws ValidationException {
        this.image = image;
    }
}
