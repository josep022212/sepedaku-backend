package com.example.sepedaku.modules.item.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdateItemDTO {
    private String category;
    private String image;
    private String description;
    private String name;
    private Integer quantity;
    private Double sellPrice;
}
