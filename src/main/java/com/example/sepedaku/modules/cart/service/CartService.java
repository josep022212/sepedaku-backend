package com.example.sepedaku.modules.cart.service;

import com.example.sepedaku.modules.cart.data.CartEntity;
import com.example.sepedaku.modules.cart.data.CartItemEntity;
import com.example.sepedaku.modules.cart.dto.*;
import com.example.sepedaku.modules.cart.repository.CartItemRepository;
import com.example.sepedaku.modules.cart.repository.CartRepository;
import com.example.sepedaku.modules.cart.vo.TotalItem;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.customer.repository.CustomerRepository;
import com.example.sepedaku.modules.item.data.ItemEntity;
import com.example.sepedaku.modules.item.dto.ItemDTO;
import com.example.sepedaku.modules.item.repository.ItemRepository;
import com.example.sepedaku.modules.item.vo.Quantity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CartService {
    private final CartRepository cartRepository;
    private final ItemRepository itemRepository;
    private final CartItemRepository cartItemRepository;
    private final CustomerRepository customerRepository;

    public CartDTO getCartItem(CustomerEntity customer) throws Exception{
        CustomerDTO customerDTO = new CustomerDTO(customer);
        CartEntity cart = cartRepository.findByCustomer(customer);
        if (cart == null){
            cart = cartRepository.saveAndFlush(new CartEntity(customer));
        }

        List<CartItemEntity> cartItems = cartItemRepository.findByCart(cart);
        List<CartItemDTO> cartItemDTOS = new ArrayList<>();
        Integer totalItem = 0;
        Float totalPrice = 0f;

        if (cartItems.size() > 0){
            for (CartItemEntity cartItem : cartItems){
                ItemDTO itemDTO = new ItemDTO(cartItem.getItem());
                itemDTO.setQuantity(cartItem.getQuantity().getQuantity());
                CartItemDTO cartItemDTO = new CartItemDTO(cartItem.getId(), itemDTO);
                cartItemDTOS.add(cartItemDTO);
                totalItem += cartItem.getQuantity().getQuantity();
                totalPrice = totalPrice + cartItem.getItem().getSellPrice().getSellPrice().floatValue() * cartItem.getQuantity().getQuantity().floatValue();
            }
        }

        return new CartDTO(cart.getId(), totalItem, totalPrice, customerDTO, cartItemDTOS);
    }

    public CartDTO save(CreateCartDTO dto) throws Exception{
        CartEntity cart = null;
        CustomerEntity customer = customerRepository.findById(dto.getCustomerId()).orElse(null);
        CustomerDTO customerDTO = new CustomerDTO(customer);
        if (customer == null){
            throw new Exception("Customer with id  " + dto.getCustomerId() + " not Found");
        }

        if (cartRepository.existsByCustomer(customer)){
            cart = cartRepository.findByCustomer(customer);
        }else{
            cart = cartRepository.saveAndFlush(new CartEntity(customer));
        }

        List<CartItemEntity> cartItems = cartItemRepository.findByCart(cart);
        List<CartItemDTO> cartItemDTOS = new ArrayList<>();
        Integer totalItem = 0;
        Float totalPrice = 0f;

        for (BuyItemDTO buyItemDTO: dto.getItems()) {
            ItemEntity item = itemRepository.getById(buyItemDTO.getItemId());
            CartItemEntity cartItem = null;
            if (cartItemRepository.existsByCartAndItem(cart, item)){
                cartItem = cartItemRepository.findByCartAndItem(cart, item);
                cartItem.setQuantity(new Quantity(cartItem.getQuantity().getQuantity() + buyItemDTO.getQuantity()));
                cartItemRepository.saveAndFlush(cartItem);
            }else{
                cartItem = new CartItemEntity(cart, item, buyItemDTO.getQuantity());
                cartItems.add(cartItem);
            }
        }

        cartItemRepository.saveAllAndFlush(cartItems);

        for (CartItemEntity cartItem : cartItems){
            ItemDTO itemDTO = new ItemDTO(cartItem.getItem());
            itemDTO.setQuantity(cartItem.getQuantity().getQuantity());
            CartItemDTO cartItemDTO = new CartItemDTO(cartItem.getId(), itemDTO);
            cartItemDTOS.add(cartItemDTO);
            totalItem += cartItem.getQuantity().getQuantity();
            totalPrice = totalPrice + cartItem.getItem().getSellPrice().getSellPrice().floatValue() * cartItem.getQuantity().getQuantity().floatValue();
        }

        cart.setItems(cartItems);
        cart.setTotalItem(new TotalItem(totalItem));
        cartRepository.saveAndFlush(cart);
        return new CartDTO(cart.getId(), totalItem, totalPrice, customerDTO, cartItemDTOS);
    }

    public CartDTO update(List<UpdateCartItemDTO> dtos, CustomerEntity customer) throws Exception{
        List<CartItemEntity> cartItems = new ArrayList<>();

        if (dtos.size() > 0){
            for (UpdateCartItemDTO dto: dtos) {
                CartItemEntity cartItem = cartItemRepository.findById(dto.getCartItemId()).orElse(null);
                if (dto.getQuantity() == 0){
                    cartItemRepository.delete(cartItem);
                }else{
                    cartItem.setQuantity(new Quantity(dto.getQuantity()));
                    cartItems.add(cartItem);
                }
            }

            cartItemRepository.saveAllAndFlush(cartItems);

            return getCartItem(customer);
        }

        return null;
    }

    public CartDTO delete(CartEntity cart) throws Exception{
        cartItemRepository.deleteByCart(cart);
        return getCartItem(cart.getCustomer());
    }

    public CartEntity getCart(CustomerEntity customer) throws Exception{
        return cartRepository.findByCustomer(customer);
    }
}
