package com.example.sepedaku.modules.cart.repository;

import com.example.sepedaku.modules.cart.data.CartEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CartRepository extends JpaRepository<CartEntity, Integer>, JpaSpecificationExecutor<CartEntity> {
    CartEntity findByCustomer(CustomerEntity customer);
    boolean existsByCustomer(CustomerEntity customer);
}
