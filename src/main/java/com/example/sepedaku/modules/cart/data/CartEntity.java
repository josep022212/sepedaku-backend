package com.example.sepedaku.modules.cart.data;
import com.example.sepedaku.base.entities.UserEntity;
import com.example.sepedaku.modules.cart.dto.CreateCartDTO;
import com.example.sepedaku.modules.cart.vo.*;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.service.CustomerService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "carts")
@SequenceGenerator(name = "cart_gen", sequenceName = "cart_id_gen", allocationSize = 1)
public class CartEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cart_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private TotalItem totalItem;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    @OneToMany(mappedBy = "cart")
    private List<CartItemEntity> items;

    public CartEntity(CustomerEntity customer){
        this.customer = customer;
    }
}
