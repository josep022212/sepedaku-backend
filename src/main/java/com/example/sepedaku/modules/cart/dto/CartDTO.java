package com.example.sepedaku.modules.cart.dto;

import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.item.dto.ItemDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartDTO {
    private Integer id;
    private Integer totalItem;
    private Float totalPrice;
    private CustomerDTO customer;
    private List<CartItemDTO> cartItems;
}
