package com.example.sepedaku.modules.cart.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CreateCartDTO {
    private Integer customerId;
    private List<BuyItemDTO> items;
}
