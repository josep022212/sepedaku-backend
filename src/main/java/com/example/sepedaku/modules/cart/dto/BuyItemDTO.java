package com.example.sepedaku.modules.cart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuyItemDTO {
    private Integer itemId;
    private Integer quantity;
}
