package com.example.sepedaku.modules.cart.dto;

import com.example.sepedaku.modules.item.dto.ItemDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CartItemDTO {
    private Integer id;
    private ItemDTO item;
}
