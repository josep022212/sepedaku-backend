package com.example.sepedaku.modules.cart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateCartItemDTO {
    private Integer cartItemId;
    private Integer quantity;
}
