package com.example.sepedaku.modules.cart.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class TotalItem {
    @Column(
            name = "total_item"
    )
    private Integer totalItem;

    public TotalItem(Integer totalItem) throws ValidationException {
        this.totalItem = totalItem;
    }
}
