package com.example.sepedaku.modules.authentication.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDTO {
    private String email;
    private String gender;
    private String name;
    private String password;
    private String phoneNumber;
    private String username;
}
