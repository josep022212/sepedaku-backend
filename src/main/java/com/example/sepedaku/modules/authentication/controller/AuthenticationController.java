package com.example.sepedaku.modules.authentication.controller;

import com.example.sepedaku.modules.authentication.dto.LoginDTO;
import com.example.sepedaku.modules.authentication.dto.RegisterDTO;
import com.example.sepedaku.modules.authentication.service.AuthenticationService;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public CustomerDTO register(
            @RequestBody RegisterDTO registerDTO
    ) throws Exception{
        return authenticationService.register(registerDTO);
    }

    @PostMapping("/login")
    public CustomerDTO login(
            @RequestBody LoginDTO dto
    ) throws Exception{
        return authenticationService.Login(dto);
    }
}
