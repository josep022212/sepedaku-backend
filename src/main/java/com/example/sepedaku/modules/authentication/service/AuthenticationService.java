package com.example.sepedaku.modules.authentication.service;

import com.example.sepedaku.modules.admin.data.AdminEntity;
import com.example.sepedaku.modules.admin.repository.AdminRepository;
import com.example.sepedaku.modules.authentication.dto.LoginDTO;
import com.example.sepedaku.modules.authentication.dto.RegisterDTO;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.customer.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;

@Service
@AllArgsConstructor
public class AuthenticationService {
    private final CustomerRepository customerRepository;
    private final AdminRepository adminRepository;

    public CustomerDTO register(RegisterDTO dto) throws Exception{
        String password = dto.getPassword();
        dto.setPassword(passwordToMD5(password));

        if(customerRepository.existsByUsername_Username(dto.getUsername()) || adminRepository.existsByUsername_Username(dto.getUsername())){
            throw new Exception("Username " + dto.getUsername() + " already exist");
        }

        CustomerEntity savedCustomer = customerRepository.saveAndFlush(new CustomerEntity(dto));
        CustomerDTO customerDTO = new CustomerDTO(savedCustomer);
        return customerDTO;
    }

    public CustomerDTO Login(LoginDTO dto) throws Exception{
        String username = dto.getUsername();
        String encryptPassword = passwordToMD5(dto.getPassword());

        if (customerRepository.existsByUsername_UsernameAndPassword_Password(username, encryptPassword)){
            CustomerEntity customer = customerRepository.findFirstByUsername_UsernameAndPassword_Password(username, encryptPassword);
            CustomerDTO customerDTO = new CustomerDTO(customer);
            return customerDTO;
        }else if(adminRepository.existsByUsername_UsernameAndPassword_Password(username, dto.getPassword())){
            AdminEntity admin = adminRepository.findFirstByUsername_UsernameAndPassword_Password(username, dto.getPassword());
            CustomerDTO customerDTO = new CustomerDTO(admin);
            return customerDTO;
        }else{
            throw new Exception("Username or Password is Incorrect");
        }
    }

    public String passwordToMD5(String passwordToHash){
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }
}
