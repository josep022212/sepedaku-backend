package com.example.sepedaku.modules.customer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdateAddressDTO {
    private String city;
    private String country;
    private String postalCode;
    private String province;
    private String street;
}
