package com.example.sepedaku.modules.customer.controller;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CreateUpdateCreditCardInfoDTO;
import com.example.sepedaku.modules.customer.dto.CreditCardInfoDTO;
import com.example.sepedaku.modules.customer.service.CreditCardInfoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class CreditCardInfoController {
    private final CreditCardInfoService creditCardInfoService;

    @PostMapping("/{userId}/ccinfo")
    public CreditCardInfoDTO save(
            @PathVariable("userId") CustomerEntity customer,
            @RequestBody CreateUpdateCreditCardInfoDTO dto
    ) throws Exception{
        return creditCardInfoService.save(dto, customer);
    }

    @GetMapping("/{userId}/ccinfo")
    public List<CreditCardInfoDTO> find(
            @PathVariable("userId") CustomerEntity customer
    ) throws Exception{
        return creditCardInfoService.find(customer);
    }

    @GetMapping("/ccinfo/{id}")
    public CreditCardInfoDTO get(
            @PathVariable("id") CreditCardInfoEntity creditCardInfo
    ) throws Exception{
        return new CreditCardInfoDTO(creditCardInfo);
    }

    @Transactional
    @PutMapping("/{userId}/ccinfo/{id}")
    public CreditCardInfoDTO Update(
            @PathVariable("userId") CustomerEntity customer,
            @PathVariable("id") CreditCardInfoEntity creditCardInfo,
            @RequestBody CreateUpdateCreditCardInfoDTO dto
    ) throws Exception{
        return creditCardInfoService.update(dto, customer, creditCardInfo);
    }

    @Transactional
    @DeleteMapping("/{userId}/ccinfo/{id}")
    public CreditCardInfoDTO delete(
            @PathVariable("userId") CustomerEntity customer,
            @PathVariable("id") CreditCardInfoEntity creditCardInfo
    )throws Exception{
        return creditCardInfoService.delete(customer, creditCardInfo);
    }

}
