package com.example.sepedaku.modules.customer.dto;

import com.example.sepedaku.base.enumerator.UserTypeEnum;
import com.example.sepedaku.modules.admin.data.AdminEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDTO {
    private Integer id;
    private String email;
    private String gender;
    private String name;
    private String phoneNumber;
    private String username;
    private Double balance;
    private String type;

    public CustomerDTO(CustomerEntity entity){
        if(entity == null) return;

        id = entity.getId();
        email = entity.getEmail().getEmail();
        gender = entity.getGender().getGender();
        name = entity.getName().getName();
        phoneNumber = entity.getPhoneNumber().getPhoneNumber();
        username = entity.getUsername().getUsername();
        balance = entity.getBalance().getBalance();
        type = UserTypeEnum.CUSTOMER.getUserType();
    }

    public CustomerDTO(AdminEntity entity){
        if(entity == null) return;

        id = entity.getId();
        email = entity.getEmail().getEmail();
        gender = entity.getGender().getGender();
        name = entity.getName().getName();
        phoneNumber = entity.getPhoneNumber().getPhoneNumber();
        username = entity.getUsername().getUsername();
        balance = null;
        type = UserTypeEnum.ADMIN.getUserType();
    }
}
