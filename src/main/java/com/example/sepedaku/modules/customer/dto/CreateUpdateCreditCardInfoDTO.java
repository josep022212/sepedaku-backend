package com.example.sepedaku.modules.customer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdateCreditCardInfoDTO {
    private String cardNumber;
    private String cardName;
    private String expirationDate;
    private String securityCode;
}
