package com.example.sepedaku.modules.customer.data;

import com.example.sepedaku.modules.customer.dto.CreateUpdateCreditCardInfoDTO;
import com.example.sepedaku.modules.customer.vo.CardName;
import com.example.sepedaku.modules.customer.vo.CardNumber;
import com.example.sepedaku.modules.customer.vo.ExpirationDate;
import com.example.sepedaku.modules.customer.vo.SecurityCode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "credit_card_infos")
@SequenceGenerator(name = "credit_card_info_gen", sequenceName = "credit_card_info_id_gen", allocationSize = 1)
public class CreditCardInfoEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "credit_card_info_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private CardNumber cardNumber;

    @Embedded
    private CardName cardName;

    @Embedded
    private ExpirationDate expirationDate;

    @Embedded
    private SecurityCode securityCode;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public CreditCardInfoEntity(
            CreateUpdateCreditCardInfoDTO dto,
            CustomerEntity customer
    ) throws Exception{
        cardNumber = new CardNumber(dto.getCardNumber());
        cardName = new CardName(dto.getCardName());
        expirationDate = new ExpirationDate(dto.getExpirationDate());
        securityCode = new SecurityCode(dto.getSecurityCode());
        this.customer = customer;
    }

    public void mapUpdate(
            CreateUpdateCreditCardInfoDTO dto
    )throws Exception{
        cardNumber = dto.getCardNumber() == null ? cardNumber : new CardNumber(dto.getCardNumber());
        cardName = dto.getCardName() == null ? cardName : new CardName(dto.getCardName());
        expirationDate = dto.getExpirationDate() == null ? expirationDate : new ExpirationDate(dto.getExpirationDate());
        securityCode = dto.getSecurityCode() == null ? securityCode : new SecurityCode(dto.getSecurityCode());
    }
}
