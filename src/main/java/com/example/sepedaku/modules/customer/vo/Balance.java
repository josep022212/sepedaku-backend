package com.example.sepedaku.modules.customer.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Balance {
    @Column(
            name = "balance"
    )
    private Double balance;

    public Balance(Double balance) throws ValidationException {
        this.balance = balance;
    }
}
