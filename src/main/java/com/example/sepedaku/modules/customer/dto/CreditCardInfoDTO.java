package com.example.sepedaku.modules.customer.dto;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCardInfoDTO {
    private Integer id;
    private String cardNumber;
    private String cardName;
    private String expirationDate;
    private CustomerDTO customer;

    public CreditCardInfoDTO(CreditCardInfoEntity entity){
        if (entity == null) return;

        id = entity.getId();
        cardNumber = entity.getCardNumber().getCardNumber();
        cardName = entity.getCardName().getCardName();
        expirationDate = entity.getExpirationDate().getExpirationDate().toString();
        this.customer = new CustomerDTO(entity.getCustomer());
    }
}
