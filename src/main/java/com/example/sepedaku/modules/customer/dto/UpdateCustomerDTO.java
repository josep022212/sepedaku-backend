package com.example.sepedaku.modules.customer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateCustomerDTO {
    private String email;
    private String gender;
    private String name;
    private String phoneNumber;
}
