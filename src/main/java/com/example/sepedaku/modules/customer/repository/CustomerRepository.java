package com.example.sepedaku.modules.customer.repository;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Integer>, JpaSpecificationExecutor<CustomerEntity> {
    boolean existsByUsername_Username(String username);

    boolean existsByUsername_UsernameAndPassword_Password(String username, String password);

    CustomerEntity findFirstByUsername_UsernameAndPassword_Password(String username, String password);


}
