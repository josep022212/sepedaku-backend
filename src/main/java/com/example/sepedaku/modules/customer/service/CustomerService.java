package com.example.sepedaku.modules.customer.service;

import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.customer.repository.CustomerRepository;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerDTO get(CustomerEntity customer){
        return new CustomerDTO(customer);
    }

    public CustomerDTO update(CustomerEntity customer){
        CustomerEntity updatedCustomer = customerRepository.saveAndFlush(customer);
        return new CustomerDTO(updatedCustomer);
    }

    public CustomerDTO delete(CustomerEntity customer){
        customerRepository.delete(customer);
        return new CustomerDTO(customer);
    }

    public List<CustomerDTO> find(Specification<CustomerEntity> specs){
        List<CustomerEntity> customers = customerRepository.findAll(specs);
        return customers
                .stream()
                .map(entity -> {
                    return new CustomerDTO(entity);
        }).collect(Collectors.toList());
    }
}
