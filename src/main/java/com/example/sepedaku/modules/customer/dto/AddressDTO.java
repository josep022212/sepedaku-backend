package com.example.sepedaku.modules.customer.dto;

import com.example.sepedaku.modules.customer.data.AddressEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDTO {
    private Integer id;
    private String city;
    private String country;
    private String postalCode;
    private String province;
    private String street;
    private CustomerDTO customer;

    public AddressDTO(AddressEntity entity){
        if (entity == null) return;

        id = entity.getId();
        city = entity.getCity().getCity();
        country = entity.getCountry().getCountry();
        postalCode = entity.getPostalCode().getPostalCode();
        province = entity.getProvince().getProvince();
        street = entity.getStreet().getStreet();
        this.customer = new CustomerDTO(entity.getCustomer());
    }

}
