package com.example.sepedaku.modules.customer.vo;
import com.example.sepedaku.exception.ValidationException;
import com.example.sepedaku.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
@Getter
@NoArgsConstructor
public class ExpirationDate {
    @Column(
            name = "expiration_date"
    )
    private Date expirationDate;

    public ExpirationDate(String expirationDate) throws ValidationException {
        if(expirationDate == null || expirationDate.trim().isEmpty()) {
            this.expirationDate = null;
        }else{
            try{
                this.expirationDate = java.sql.Date.valueOf(expirationDate);
            }catch (Exception e){
                throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_VALUE_INVALID.getMessage(),"date"));
            }
        }
    }
}
