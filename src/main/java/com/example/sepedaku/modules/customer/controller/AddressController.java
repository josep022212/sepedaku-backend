package com.example.sepedaku.modules.customer.controller;

import com.example.sepedaku.modules.customer.data.AddressEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.AddressDTO;
import com.example.sepedaku.modules.customer.dto.CreateUpdateAddressDTO;
import com.example.sepedaku.modules.customer.service.AddressService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{userId}/addresses")
    public AddressDTO save(
            @PathVariable("userId") CustomerEntity customer,
            @RequestBody CreateUpdateAddressDTO dto
    ) throws Exception{
        return addressService.save(dto, customer);
    }

    @GetMapping("/{userId}/addresses")
    public List<AddressDTO> find(
            @PathVariable("userId") CustomerEntity customer
    ) throws Exception{
        return addressService.find(customer);
    }

    @GetMapping("/addresses/{id}")
    public AddressDTO get(
            @PathVariable("id") AddressEntity address
    ) throws Exception{
        return new AddressDTO(address);
    }

    @Transactional
    @PutMapping("/addresses/{id}")
    public AddressDTO Update(
            @PathVariable("id") AddressEntity address,
            @RequestBody CreateUpdateAddressDTO dto
    ) throws Exception{
        return addressService.update(dto, address);
    }

    @Transactional
    @DeleteMapping("/addresses/{id}")
    public AddressDTO delete(
            @PathVariable("id") AddressEntity address
    )throws Exception{
        return addressService.delete(address);
    }
}
