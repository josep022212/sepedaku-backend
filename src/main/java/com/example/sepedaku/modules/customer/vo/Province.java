package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Province {
    @Column(
            name = "province"
    )
    private String province;

    public Province(String province) throws ValidationException {
        this.province = province;
    }
}
