package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Country {
    @Column(
            name = "country"
    )
    private String country;

    public Country(String country) throws ValidationException {
        this.country = country;
    }
}
