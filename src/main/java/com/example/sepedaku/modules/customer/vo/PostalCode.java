package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class PostalCode {
    @Column(
            name = "postal_code"
    )
    private String postalCode;

    public PostalCode(String postalCode) throws ValidationException {
        this.postalCode = postalCode;
    }
}
