package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Street {
    @Column(
            name = "street"
    )
    private String street;

    public Street(String street) throws ValidationException {
        this.street = street;
    }
}
