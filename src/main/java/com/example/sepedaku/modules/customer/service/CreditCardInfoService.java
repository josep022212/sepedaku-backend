package com.example.sepedaku.modules.customer.service;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CreateUpdateCreditCardInfoDTO;
import com.example.sepedaku.modules.customer.dto.CreditCardInfoDTO;
import com.example.sepedaku.modules.customer.repository.CreditCardInfoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CreditCardInfoService {
    private final CreditCardInfoRepository creditCardInfoRepository;

    public CreditCardInfoDTO save(CreateUpdateCreditCardInfoDTO dto, CustomerEntity customer) throws Exception{
        CreditCardInfoEntity creditCardInfo = creditCardInfoRepository.saveAndFlush(
                new CreditCardInfoEntity(dto, customer)
        );
        return new CreditCardInfoDTO(creditCardInfo);
    }

    public List<CreditCardInfoDTO> find(CustomerEntity customer){
        List<CreditCardInfoEntity> creditCardInfos = creditCardInfoRepository.findByCustomer(customer);
        return creditCardInfos
                .stream()
                .map(entity -> {
                    CreditCardInfoDTO dto = new CreditCardInfoDTO(entity);
                    return dto;
                }).collect(Collectors.toList());
    }

    public CreditCardInfoDTO update(CreateUpdateCreditCardInfoDTO dto, CustomerEntity customer, CreditCardInfoEntity creditCardInfo) throws Exception{
        creditCardInfo.mapUpdate(dto);
        return new CreditCardInfoDTO(creditCardInfo);
    }

    public CreditCardInfoDTO delete(CustomerEntity customer, CreditCardInfoEntity creditCardInfo) throws Exception{
        CreditCardInfoDTO creditCardInfoDTO = new CreditCardInfoDTO(creditCardInfo);
        creditCardInfoRepository.delete(creditCardInfo);
        return creditCardInfoDTO;
    }
}
