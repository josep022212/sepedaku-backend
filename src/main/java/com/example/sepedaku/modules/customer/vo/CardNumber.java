package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class CardNumber {
    @Column(
            name = "card_number"
    )
    private String cardNumber;

    public CardNumber(String cardNumber) throws ValidationException {
        this.cardNumber = cardNumber;
    }
}
