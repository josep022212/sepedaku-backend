package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class CardName {
    @Column(
            name = "card_name"
    )
    private String cardName;

    public CardName(String cardName) throws ValidationException {
        this.cardName = cardName;
    }
}
