package com.example.sepedaku.modules.customer.repository;

import com.example.sepedaku.modules.customer.data.CreditCardInfoEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CreditCardInfoRepository extends JpaRepository<CreditCardInfoEntity, Integer>, JpaSpecificationExecutor<CreditCardInfoEntity> {
    List<CreditCardInfoEntity> findByCustomer(CustomerEntity customer);

}
