package com.example.sepedaku.modules.customer.service;

import com.example.sepedaku.modules.customer.data.AddressEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.AddressDTO;
import com.example.sepedaku.modules.customer.dto.CreateUpdateAddressDTO;
import com.example.sepedaku.modules.customer.repository.AddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressDTO save(CreateUpdateAddressDTO dto, CustomerEntity customer) throws Exception{
        AddressEntity address = new AddressEntity(dto, customer);
        return new AddressDTO(addressRepository.saveAndFlush(address));
    }

    public List<AddressDTO> find(CustomerEntity customer) throws Exception{
        List<AddressEntity> addresses = addressRepository.findByCustomer(customer);
        return addresses
                .stream()
                .map(entity -> {
                    return new AddressDTO(entity);
                }).collect(Collectors.toList());
    }

    public AddressDTO update(CreateUpdateAddressDTO dto, AddressEntity address) throws Exception{
        address.mapUpdate(dto);
        return new AddressDTO(addressRepository.saveAndFlush(address));
    }

    public AddressDTO delete(AddressEntity address) throws Exception{
        AddressDTO dto = new AddressDTO(address);
        addressRepository.delete(address);
        return dto;
    }
}
