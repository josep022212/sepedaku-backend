package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class SecurityCode {
    @Column(
            name = "security_code"
    )
    private String securityCode;

    public SecurityCode(String securityCode) throws ValidationException {
        this.securityCode = securityCode;
    }
}
