package com.example.sepedaku.modules.customer.data;

import com.example.sepedaku.modules.customer.dto.CreateUpdateAddressDTO;
import com.example.sepedaku.modules.customer.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "addresses")
@SequenceGenerator(name = "addresses_gen", sequenceName = "addresses_id_gen", allocationSize = 1)
public class AddressEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "addresses_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private City city;

    @Embedded
    private Country country;

    @Embedded
    private PostalCode postalCode;

    @Embedded
    private Province province;

    @Embedded
    private Street street;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    public AddressEntity(
            CreateUpdateAddressDTO dto,
            CustomerEntity customer
    ) throws Exception{
        city = new City(dto.getCity());
        country = new Country(dto.getCountry());
        postalCode = new PostalCode(dto.getPostalCode());
        province = new Province(dto.getProvince());
        street = new Street(dto.getStreet());
        this.customer = customer;
    }

    public void mapUpdate(
            CreateUpdateAddressDTO dto
    ) throws Exception{
        city = dto.getCity() == null ? city : new City(dto.getCity());
        country = dto.getCountry() == null ? country : new Country(dto.getCountry());
        postalCode = dto.getPostalCode() == null ? postalCode : new PostalCode(dto.getPostalCode());
        province = dto.getProvince() == null ? province : new Province(dto.getProvince());
        street = dto.getStreet() == null ? street : new Street(dto.getStreet());
    }
}