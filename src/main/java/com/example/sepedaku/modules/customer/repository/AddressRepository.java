package com.example.sepedaku.modules.customer.repository;

import com.example.sepedaku.modules.customer.data.AddressEntity;
import com.example.sepedaku.modules.customer.data.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface AddressRepository extends JpaRepository<AddressEntity, Integer>, JpaSpecificationExecutor<AddressEntity> {
    List<AddressEntity> findByCustomer(CustomerEntity customer);

}
