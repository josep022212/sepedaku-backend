package com.example.sepedaku.modules.customer.data;

import com.example.sepedaku.base.entities.UserEntity;
import com.example.sepedaku.base.vo.*;
import com.example.sepedaku.modules.authentication.dto.RegisterDTO;
import com.example.sepedaku.modules.customer.dto.UpdateCustomerDTO;
import com.example.sepedaku.modules.customer.vo.Balance;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "customers")
@SequenceGenerator(name = "customer_gen", sequenceName = "customer_id_gen", allocationSize = 1)
public class CustomerEntity extends UserEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "customer_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded
    private Balance balance;

    @OneToMany(mappedBy = "customer")
    private List<CreditCardInfoEntity> creditCardInfos;

    @OneToMany(mappedBy = "customer")
    private List<AddressEntity> addresses;

    @OneToMany(mappedBy = "customer")
    private List<SalesOrderEntity> salesOrders;

    //registration constructor
    public CustomerEntity(
        RegisterDTO dto
    ) throws Exception {
        email = new Email(dto.getEmail());
        gender = new Gender(dto.getGender());
        name = new Name(dto.getName());
        password = new Password(dto.getPassword());
        phoneNumber = new PhoneNumber(dto.getPhoneNumber());
        username = new Username(dto.getUsername());
        balance = new Balance(0.0);
    }

    public void mapUpdate(
            UpdateCustomerDTO dto
    ) throws Exception{
        email = dto.getEmail() == null ? email : new Email(dto.getEmail());
        gender = dto.getGender() == null ? gender : new Gender(dto.getGender());
        name = dto.getName() == null ? name : new Name(dto.getName());
        phoneNumber = dto.getPhoneNumber() == null ? phoneNumber : new PhoneNumber(dto.getPhoneNumber());
    }
}
