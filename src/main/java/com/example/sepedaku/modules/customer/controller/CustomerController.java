package com.example.sepedaku.modules.customer.controller;

import com.example.sepedaku.modules.customer.data.CustomerEntity;
import com.example.sepedaku.modules.customer.dto.CustomerDTO;
import com.example.sepedaku.modules.customer.dto.UpdateCustomerDTO;
import com.example.sepedaku.modules.customer.service.CustomerService;
import com.example.sepedaku.modules.sales.data.SalesOrderEntity;
import com.sipios.springsearch.anotation.SearchSpec;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping("/{id}")
    public CustomerDTO get(
            @PathVariable("id")CustomerEntity customer
    ) throws Exception{
        return customerService.get(customer);
    }

    @GetMapping()
    public List<CustomerDTO> find(
            @SearchSpec Specification<CustomerEntity> specs
    ) throws Exception{
        return customerService.find(specs);
    }

    @PutMapping("/{id}")
    public CustomerDTO update(
            @PathVariable("id")CustomerEntity customer,
            @RequestBody UpdateCustomerDTO dto
    ) throws Exception{
        customer.mapUpdate(dto);
        return customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    public CustomerDTO delete(
            @PathVariable("id")CustomerEntity customer
    ) throws Exception{
        return customerService.delete(customer);
    }
}
