package com.example.sepedaku.modules.customer.vo;

import com.example.sepedaku.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class City {
    @Column(
            name = "city"
    )
    private String city;

    public City(String city) throws ValidationException {
        this.city = city;
    }
}
