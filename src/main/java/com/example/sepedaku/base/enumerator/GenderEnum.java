package com.example.sepedaku.base.enumerator;

import lombok.Getter;

@Getter
public enum GenderEnum {
    MALE("MALE"), FEMALE("FEMALE");
    private String genderEnum;

    GenderEnum(String gender){
        this.genderEnum = gender;
    }

    public static GenderEnum validateGender(String gender) {
        switch (gender) {
            case "MALE":
                return GenderEnum.MALE;

            case "FEMALE":
                return GenderEnum.FEMALE;

            default:
                throw new IllegalArgumentException("gender [" + gender  + "] not supported.");
        }
    }
}
