package com.example.sepedaku.base.enumerator;

import lombok.Getter;

@Getter
public enum UserTypeEnum {
    CUSTOMER("CUSTOMER"), ADMIN("ADMIN");
    private String userType;

    UserTypeEnum(String userType){
        this.userType = userType;
    }

    public static UserTypeEnum validateUserType(String userType) {
        switch (userType) {
            case "CUSTOMER":
                return UserTypeEnum.CUSTOMER;

            case "ADMIN":
                return UserTypeEnum.ADMIN;

            default:
                throw new IllegalArgumentException("User type [" + userType  + "] not supported.");
        }
    }
}
