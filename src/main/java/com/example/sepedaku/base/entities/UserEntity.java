package com.example.sepedaku.base.entities;

import com.example.sepedaku.base.vo.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
    @Embedded
    protected Email email;
    @Embedded
    protected Gender gender;
    @Embedded
    protected Name name;
    @Embedded
    protected Password password;
    @Embedded
    protected PhoneNumber phoneNumber;
    @Embedded
    protected Username username;
}
