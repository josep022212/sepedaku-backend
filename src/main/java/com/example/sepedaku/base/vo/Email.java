package com.example.sepedaku.base.vo;

import com.example.sepedaku.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Embeddable
@Getter
@NoArgsConstructor
public class Email {
    @Column(
            name = "email"
    )
    private String email;

    public Email(String email) throws ValidationException {
        if (email != null) {
            // Email validation : https://blog.mailtrap.io/java-email-validation/
            final String regex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches())
                throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_VALUE_INVALID.getMessage(), "Email"));

        }
        this.email = email;
    }
}
