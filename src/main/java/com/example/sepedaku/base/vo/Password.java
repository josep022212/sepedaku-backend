package com.example.sepedaku.base.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Password {
    @Column(
            name = "password"
    )
    private String password;

    public Password(String password) throws ValidationException {
        this.password = password;
    }
}
