package com.example.sepedaku.base.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class PhoneNumber {
    @Column(
            name = "phone_number"
    )
    private String phoneNumber;

    public PhoneNumber(String phoneNumber) throws ValidationException {
        this.phoneNumber = phoneNumber;
    }
}
