package com.example.sepedaku.base.vo;

import com.example.sepedaku.base.enumerator.GenderEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Gender {
    @Column(
            name = "gender"
    )
    private String gender;

    public Gender(String gender) throws ValidationException {
        gender = GenderEnum.validateGender(gender).getGenderEnum();
        this.gender = gender;
    }
}
