package com.example.sepedaku.base.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.example.sepedaku.exception.ValidationException;

@Embeddable
@Getter
@NoArgsConstructor
public class Username {
    @Column(
            name = "username"
    )
    private String username;

    public Username(String username) throws ValidationException {
        this.username = username;
    }
}
