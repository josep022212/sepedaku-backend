package com.example.sepedaku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SepedakuApplication {

    public static void main(String[] args) {
        SpringApplication.run(SepedakuApplication.class, args);
    }

}
